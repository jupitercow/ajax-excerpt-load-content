<?php
/*
Plugin Name: AJAX Excerpt Load Content
Plugin URI: http://bitbucket.org/jupitercow/ajax-excerpt-load-content
Description: Loads the rest of the content in place of the excerpt without page refresh. Demonstration for WordCamp presentation.
Version: 1.0.0
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

/**
 * Replace "[...]" with a "Read more" link
 *
 * @param	string $more The default excerpt more
 * @return	string The custom Read more link for ajax with a fallback to go to the post when javascript isn't working
 */
if (! function_exists('aelc_excerpt_more') ) :

add_filter( 'excerpt_more', 'aelc_excerpt_more' );
function aelc_excerpt_more( $more )
{
	// Get the current post
	global $post;

	// Create the new link
	$output  = "&hellip;";
	$output .= '<br />';
	$output .= '<a class="excerpt-read-more" data-post_id="'. $post->ID .'" href="'. get_permalink($post->ID) . '" title="Read ' . get_the_title($post->ID).'">';
	$output .= "Read more";
	$output .= '</a>';

	// Return it to the original function
	return $output;
}

endif;

/**
 * Enqueue the Ajax script
 *
 * @return	void
 */
if (! function_exists('aelc_enqueue_scripts') ) :

add_action( 'wp_enqueue_scripts', 'aelc_enqueue_scripts', 999 );
function aelc_enqueue_scripts()
{
	// Register our js to load in the footer
	// Tell WP to make sure jQuery is also loaded first
	wp_register_script( 'aelc-js', plugins_url( 'assets/js/scripts.js', __FILE__ ), array( 'jquery' ), '', true );

	// Register our variables with Javascript through script localization
	$args = array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'nonce'   => wp_create_nonce( 'aelc_excerpt_more' ),
	);
	wp_localize_script( 'aelc-js', 'aelc', $args );

	// Enqueue the script to be loaded
	wp_enqueue_script( 'aelc-js' );
}

endif;

/**
 * The Ajax handler
 *
 * Gets the content from the database and outputs it to the javascript
 *
 * @return	void
 */
if (! function_exists('aelc_load_content') ) :

add_action( 'wp_ajax_aelc_excerpt_more', 'aelc_load_content' );
add_action( 'wp_ajax_nopriv_aelc_excerpt_more', 'aelc_load_content' );
function aelc_load_content()
{
	// Make sure a post id was provided and a nonce
	if ( empty($_POST['post_id']) || empty($_POST['nonce']) ) die('empty');
	// Verify the nonce is correct
	if (! wp_verify_nonce( $_POST['nonce'], 'aelc_excerpt_more' ) ) die('bad nonce bad');

	// Get the post
	$post = get_post($_POST['post_id']);
	// Apply the_content filters to add proper formatting
	echo apply_filters( 'the_content', $post->post_content );
	// Die, so that admin-ajax.php doesn't return false
	die;
}

endif;