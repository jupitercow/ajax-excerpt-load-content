jQuery(document).ready(function($) {

	// Create an onClick event to trigger Ajax
	$('.excerpt-read-more').on('click', function(e) {
		// Keep the link from causing the browser to go to the actual page
		// This overrides the href="..."
		e.preventDefault();

		// Cache a couple variables that we will be using
		var $this = $(this),
			$content = $this.closest('p');

		// Start the Ajax
		$.ajax({
			// The ajax url: /wp-admin/admin-ajax.php
			url:      aelc.ajaxurl,
			// Submit the data via $_POST
			type:     'post',
			// Make sure it is loading asynchronously
			async:    true,
			// Don't cache the response, Ajax usually does want to be cached
			cache:    false,
			// Says we expect to get HTML in return from the handler
			dataType: 'html',
			// Data gets passed via $_POST to the PHP handler
			data: {
				// PHP handler action name (without the "wp_ajax_" part
				'action'  : 'aelc_excerpt_more',
				// The post id from the link html
				'post_id' : $this.data('post_id'),
				// The nonce
				'nonce'   : aelc.nonce,
			},

			// Upon success load our content from the response
			success: function( response )
			{
				if ( response )
				{
					// Create a container to fade in the new content
					var $new_containter = $('<div />');

					// Add the response to the container
					$new_containter.html( response );

					// Fade the content out, add the new content and fade it in
					$content.fadeOut('400', function(){
						$content
							.after( $new_containter.fadeIn() )
							.remove();
					});
				}
			}
		});
	});

});